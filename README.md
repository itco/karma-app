# Karma app #

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/itco/karma-app/badge)](https://www.codefactor.io/repository/bitbucket/itco/karma-app)

This is an application based on the CakePHP framework. Everything is included, except for the 'vendor' directory, which is the core of the framework.
In order to start working with this app, put the source in the desired directory, download the cakephp files of the right version from the [CakePHP github](github.com/cakephp/cakephp/releases) and put the 'vendor' folder among the app files.


## How to deploy ##

1. Either `hg clone [repository url]` or simply download/extract the whole repository into place. Mind any sub directories that may or may not be created. The app specific files are now into place.

2. The framework core files have to be placed now. The program/package composer will take care of this. If composer is installed globally, simply run `composer update` from the root of the application. If it is not installed globally, [download](https://getcomposer.org/download/) `composer.phar` into the root directory and run the same command.
If there are problems with e.g. permissions, take a look at the [CakePHP documentation](https://book.cakephp.org/3.0/en/installation.html).

3. Configure the database. Create a database, maybe together will a specific user. Import the tables using one of the `parem_*.sql` files in the root.

4. Set up your configuration. In your deployment, copy `config/app.default.php` to `config/app.php` and change some values in the new file. Most important are the database and credentials listed under 'datasources'.

## Authors

 * Initial development: Robert Roos <robert.soor@gmail.com>
 * Additional features: Bas Koster
 * Maintenance: Dries Cavelaars
