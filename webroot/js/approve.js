function setApprove(karma_id, state) {

    $.ajax({
        type: "POST",
        url: webroot + "karma/set-approve",
        data: {"karma_id": karma_id, "state": state},
        success: function (data, textStatus, xhr) {

            if (xhr.status === 200) {

                alert('Good connection, but no changes saved, try again');

            } else if (xhr.status === 201) {

                var row = $("#karma-entry-" + karma_id);
                
                row.removeClass();
                
                console.log(state);
                
                if (state === 0)                
                    row.addClass("waiting");
                else if (state === 2)
                    row.addClass("denied");
                
            }
        },
        error: function (xhr) {

            alert('Failed to make change');
        }
    });

}