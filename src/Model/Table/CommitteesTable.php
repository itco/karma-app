<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Committees Model
 *
 * @property \Cake\ORM\Association\HasMany $Karma
 * @property \Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Committee get($primaryKey, $options = [])
 * @method \App\Model\Entity\Committee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Committee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Committee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Committee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Committee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Committee findOrCreate($search, callable $callback = null)
 */
class CommitteesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('committees');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Karma', [
            'foreignKey' => 'committee_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'committee_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    /*public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }*/
}
