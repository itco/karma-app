<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Committees Controller
 *
 * @property \App\Model\Table\CommitteesTable $Committees
 */
class CommitteesController extends AppController
{

    //====================================================================
    public function isAuthorized($user)
    {
        if (isset($user))
        {
            if (in_array($this->request->params['action'], ['table', 'index', 'view']))
                return true;

            if (in_array($this->request->getParam('action'), ['add','edit']) && ($user['role'] =='editor')) {
                return true;
            }
        }

        // By default, each location is off-limits.
        return parent::isAuthorized($user);
    }

    //====================================================================
    public function index()
    {
        $this->set('currentMenu', 'Man_Committees');

        $committees = $this->paginate($this->Committees);

        $this->set(compact('committees'));
    }

    //====================================================================
    public function add()
    {
        $this->set('currentMenu', 'Man_Committees');

        $committee = $this->Committees->newEntity();
        if ($this->request->is('post'))
        {
            $committee = $this->Committees->patchEntity($committee, $this->request->data);
            if ($this->Committees->save($committee))
            {
                $this->Flash->success(__('The case has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('The case could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('committee'));
    }

    //====================================================================
    public function edit($id = null)
    {
        $this->set('currentMenu', 'Man_Committees');

        $committee = $this->Committees->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put']))
        {
            $committee = $this->Committees->patchEntity($committee, $this->request->data);
            if ($this->Committees->save($committee))
            {
                $this->Flash->success(__('The case has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('The case could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('committee'));
    }

    //====================================================================
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $committee = $this->Committees->get($id);
        if ($this->Committees->delete($committee))
        {
            $this->Flash->success(__('The case has been deleted.'));
        }
        else
        {
            $this->Flash->error(__('The case could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //====================================================================
    public function table()
    {
        $this->set('currentMenu', 'Committees');

        $committees = $this->Committees->find('all')
                ->select($this->Committees)
                ->leftJoinWith('Users')
                ->contain('Karma')
                ->group('Committees.id')
                ->toArray();

        foreach ($committees as $committee)
        {
            $committee->totalkarma = 0;

            foreach ($committee->karma as $karma)
            {
                if ($karma['approved'] == 1)
                {
                    $committee->totalkarma += $karma->points;
                }
            }
        }

        $this->set(compact('committees'));
    }

    //====================================================================
    public function view($id = NULL)
    {
        $this->set('currentMenu', 'Committees');

        if (is_null($id))
        {
            $id = $this->Auth->user('committee_id');
        }

        $committee = $this->Committees->get($id);
        $this->set(compact('committee'));

        $this->paginate = [
            'limit' => 10,
            'order' => ['Karma.date' => 'desc'],
            'sortWhitelist' => ['Users.firstname', 'Karma.points', 'Karma.comment', 'Karma.date'],
        ];

        $this->loadModel('Karma');
        $karmas = $this->Karma->find('all')
                ->contain(['Users'])
                ->where(['Karma.committee_id' => $id])
                ->andWhere(['Karma.approved' => 1]);

        $this->set('karmas', $this->paginate($karmas));
    }

}
