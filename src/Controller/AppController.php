<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    //====================================================================
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        
        //Set-up authentication
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginRedirect' => ['controller' => 'Karma', 'action' => 'view'],
            'authError' => 'You need to be logged in or you lack access rights. Something wrong? Contact board@ssa-ut.nl',
            'logoutRedirect' => ['controller' => 'Users', 'action' => 'login'],
            'loginAction' => ['controller' => 'Users', 'action' => 'login'],
        ]);

        //Config cookies
        $this->loadComponent('Cookie', [
            'expires' => '+2 weeks',
            'httpOnly' => true
        ]);
        
        $this->set('authUser', $this->Auth->user());
        
        $this->viewBuilder()->layout('standard');
    }
    //====================================================================
    
    //====================================================================
    public function beforeFilter(Event $event)
    {
        //Login via Cookie
        if (!$this->Auth->user() && $this->Cookie->check('remember_me_cookie'))
        {
            $user_id = $this->checkSessionCookie();
            if ($user_id)
            {
                if (!$this->forceLogin($user_id))
                    $this->Flash->error('Failed to login with proper cookie');
                else
                    $this->set('authUser', $this->Auth->user());
            }
        }

        //Public actions
        //$this->Auth->allow(['index']);
    }

    //====================================================================
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    
    //====================================================================
    public function isAuthorized($user)
    {
        // 'Superuser' settings
        if (isset($user) && $user['role'] === 'admin')
        {
            return true;
        }

        // By default each location is off-limits.
        return false;
    }
    
    //====================================================================
    //Check the session cookie
    protected function checkSessionCookie()
    {
        //Read cookie
        $cookie = $this->Cookie->read('remember_me_cookie');
        $this->Cookie->delete('remember_me_cookie');

        $this->LoadModel('SessionKeys');

        //Check the identifier
        try
        {
            $sessionKey = $this->SessionKeys->get($cookie['identifier']);
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException $e)
        {
            if (Configure::read('debug'))
                $this->Flash->error('Session key not found in database');
            return false;
        }

        //Check the token
        if (!(Security::hash($cookie['token'], 'sha256', true) == $sessionKey->token))
        {
            if (Configure::read('debug'))
                $this->Flash->error('Saved token does not match the database');
            if (!$this->SessionKeys->delete($sessionKey))
            {
                if (Configure::read('debug'))
                    $this->Flash->error('Failed to remove session in database');
            }
            return false;
        }

        //Update token
        $token = $this->randomString(64);
        $this->Cookie->write('remember_me_cookie', [
            'identifier' => $cookie['identifier'],
            'token' => $token
        ]);

        $sessionKey->token = Security::hash($token, 'sha256', true);
        $this->SessionKeys->save($sessionKey);

        return $sessionKey->user_id;
    }
    
    //====================================================================
    //Impersonate a user
    protected function forceLogin($user_id = NULL)
    {
        $this->LoadModel('Users');
        try
        {
            $userR = $this->Users->get($user_id);
        }
        catch (\Cake\Datasource\Exception\RecordNotFoundException $e)
        {
            $this->Flash->error('User not found');
            return false;
        }
        
        $user = $userR->toArray();

        $this->Auth->setUser($user);

        return true;
    }
    
    //====================================================================
    //Generate a random string
    protected function randomString($length = 1)
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $rString = '';

        for ($i = 0; $i < $length; $i++)
        {
            $rString .= $chars[rand(0, strlen($chars) - 1)];
        }

        return $rString;
    }
}
