<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Cake\Mailer\Email;

class UsersController extends AppController
{

    //====================================================================
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['add', 'logout', 'forgotPassword', 'resetPassword']);
    }

    //====================================================================
    public function isAuthorized($user)
    {
        if (isset($user))
        {
            if (in_array($this->request->params['action'], ['table', 'changepassword']))
                return true;
            elseif (in_array($this->request->params['action'], ['approve']))
            {
                if (isset($user['role']) && in_array($user['role'], ['admin', 'editor']))
                    return true;
            }
            elseif ($this->request->params['action'] == 'edit')
            {
                if (!isset($this->request->pass[0]) || $this->request->pass[0] == $user['id'])
                    return true;
            }
            elseif ($this->request->params['action'] == 'delete')
            {
                if (!isset($this->request->pass[0]) || $this->request->pass[0] == $user['id'])
                    return true;
            }
        }

        // By default each location is off-limits.
        return parent::isAuthorized($user);
    }

    //====================================================================
    public function index()
    {
        $this->set('currentMenu', 'Man_Users');
        $users = $this->paginate($this->Users->find('all')->contain('Committees'));
        $this->set('users', $users);
    }

    //====================================================================
    //List all users and karma
    public function table()
    {
        $this->set('currentMenu', 'Participants');

        $this->paginate = [
            'limit' => 15,
            'order' => ['firstname' => 'asc'],
            'sortWhitelist' => ['Users.firstname', 'firstname', 'Users.lastname', 'lastname', 'karmascore']
        ];

        $participants = $this->Users->find()
                ->select($this->Users)
                ->select(['karmascore' => 'COALESCE(SUM(karma.points), 0)'])
                ->where(['karma.approved' => 1])
                ->leftJoin('karma', 'Users.id = karma.user_id')
                ->group('karma.user_id');


        $this->set('participants', $this->paginate($participants));
    }

    //====================================================================
    //New User
    public function add()
    {
        $this->set('currentMenu', 'Sign up');

        if ($this->Auth->user())
        {
            $this->Flash->set('Already logged in');
            return $this->redirect(['controller' => 'Karma', 'action' => 'view']);
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post'))
        {
            $this->request->data['role'] = 'user';
            $user = $this->Users->newEntity($this->request->data);
            if ($user->errors())
            {
                $val_errors = $user->errors();

                foreach ($val_errors as $key => $val_error)
                {
                    foreach ($val_error as $error)
                    {
                        $this->Flash->error($key . ' - ' . $error);
                    }
                }
            }
            elseif ($this->Users->save($user))
            {
                $this->Flash->success('User is saved.');
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
            else
            {
                $this->Flash->error('User could not be saved, please try again');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        $committees = $this->Users->Committees->find('list');
        $this->set(compact('committees'));
    }

    //====================================================================
    //Delete User
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if (is_null($id))
            $id = $this->Auth->user('id');

        $user = $this->Users->get($id);

        if ($this->Users->delete($user))
        {
            $this->Flash->success('User deleted.');
            if ($id == $this->Auth->user('id'))
                return $this->redirect(['controller' => 'Users', 'action' => 'logout']);
            else
                return $this->redirect('/');
        }
    }

    //====================================================================
    //Log a user in
    public function login()
    {
        if ($this->Auth->user())
        {
            return $this->redirect(['controller' => 'Karma', 'action' => 'view']);
        }

        if ($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if ($user)
            {
                $this->Auth->setUser($user);

                if ($this->request->data['remember_me'])
                {
                    if (!$this->createSessionCookie())
                    {
                        $this->Flash->error('Remember-me failed');
                    }
                }

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Wrong username or password');
        }

        $this->set('currentMenu', 'Login');
    }

    //====================================================================
    public function logout()
    {

        if ($this->Cookie->check('remember_me_cookie'))
            $this->Cookie->delete('remember_me_cookie');

        $this->loadModel('SessionKeys');
        $sessionKey = $this->SessionKeys->find('all')
                ->where(['user_id' => $this->Auth->user('id')])
                ->first();
        if (!is_null($sessionKey))
            if (!$this->SessionKeys->delete($sessionKey) && Configure::read('debug'))
                $this->Flash->error('Failed to delete session key from database');

        return $this->redirect($this->Auth->logout());
    }

    //====================================================================
    //Toggle the approved status of a user
    public function changepassword()
    {
        if (!$this->Auth->user())
        {
            return $this->redirect(['controller' => 'Karma', 'action' => 'view']);
        }

        $this->set('currentMenu', 'Change Password');

        if ($this->request->is(['post', 'patch']))
        {
            $this->request->data['username'] = $this->Auth->user('username');

            $user = $this->Users->get($this->Auth->user('id'));
            if ($this->Auth->identify())
            {
                if ($this->request->data['newpassword'] == $this->request->data['confirm_newpassword'])
                {
                    $user = $this->Users->PatchEntity($user, ['password' => $this->request->data['newpassword']]);
                    if ($this->Users->save($user))
                    {
                        $this->Flash->success('Successfully changed password');
                        return $this->redirect(['controller' => 'Karma', 'action' => 'view']);
                    }
                    else
                        $this->Flash->error('Failed to save change, please try again');
                }
                else
                {
                    $this->Flash->error('New password does not match');
                }
            }
            else
            {
                $this->Flash->error('Current password is not correct');
            }
        }
    }

    //====================================================================
    //Edit your own profile
    public function edit($id = null)
    {
        if (is_null($this->Auth->user()))
        {
            $this->Flash->error('You need to be logged in first');
            return $this->redirect('/');
        }

        if (is_null($id))
        {
            $id = $this->Auth->user('id');
            $this->set('currentMenu', 'Edit');
        }
        else
            $this->set('currentMenu', 'Man_Users');


        $user = $this->Users->get($id);

        if ($this->request->is(['post', 'patch', 'put']))
        {
            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user))
            {
                $this->Flash->success('Changed profile');
                return $this->redirect('/');
            }
            else
                $this->Flash->error('Failed to edit profile');
        }

        $this->set(compact('user'));

        $committees = $this->Users->Committees->find('list');
        $this->set(compact('committees'));
    }

    //====================================================================
    //Manage the remember-me function
    protected function createSessionCookie()
    {
        $token = $this->randomString(64);

        // Add to database
        $this->loadModel('SessionKeys');

        $sessionKey = $this->SessionKeys->find('all')
                ->where(['user_id' => $this->Auth->user('id')])
                ->first();

        if (!is_null($sessionKey))
        {
            $sessionKey = $this->SessionKeys->patchEntity($sessionKey, [
                'token' => Security::hash($token, 'sha256', true)
            ]);
        }
        else
        {
            $sessionKey = $this->SessionKeys->newEntity();
            $this->SessionKeys->patchEntity($sessionKey, [
                'identifier' => $this->randomString(64),
                'token' => Security::hash($token, 'sha256', true),
                'user_id' => $this->Auth->user('id')
            ]);
        }

        if (!$this->SessionKeys->save($sessionKey))
        {
            return false;
        }

        // Write Cookie
        $this->Cookie->write('remember_me_cookie', [
            'identifier' => $sessionKey->identifier,
            'token' => $token
        ]);

        return true;
    }

    //====================================================================
    // Forgot password panel
    public function forgotPassword()
    {
        require_once(ROOT . DS . 'src' . DS . 'recaptchalib.php'); // Get reCAPTCHA library

        $this->set('currentMenu', 'Login');

        $this->set('siteKey', Configure::read('recaptcha.siteKey'));

        if ($this->request->is('post'))
        {
            // reCaptcha check ==========================
            $resp = null;
            $reCaptcha = new \ReCaptcha(Configure::read('recaptcha.secret'));

            if ($this->request->data['g-recaptcha-response'])
            {
                $resp = $reCaptcha->verifyResponse(
                        $this->request->clientIp(), $this->request->data['g-recaptcha-response']
                );
            }

            if ($resp == null || !$resp->success)
            {
                $this->Flash->error('Captcha failed');
                return;
            }
            //===========================================

            $user = $this->Users->find()
                    ->where(['email' => $this->request->data['email']])
                    ->first();

            if (is_null($user))
            {
                $this->Flash->error('No user found with that email');
            }
            else
            {
                $key = $this->Users->SessionKeys->find()
                        ->where(['user_id' => $user->id])
                        ->first();

                $token = $this->randomString(64);

                if (!is_null($key))
                {
                    $key = $this->Users->SessionKeys->PatchEntity($key, [
                        'token' => Security::hash($token, 'sha256', true),
                    ]);
                }
                else
                {
                    $key = $this->Users->SessionKeys->newEntity([
                        'identifier' => $this->randomString(64),
                        'token' => Security::hash($token, 'sha256', true),
                        'user_id' => $user->id
                    ]);
                }


                if ($this->Users->SessionKeys->save($key))
                {

                    $email = new Email('mail');
                    $email->to($user->email)
                            ->subject('Password Reset')
                            ->template('password')
                            ->emailFormat('html')
                            ->viewVars([
                                'identifier' => $key->identifier,
                                'token' => $token,
                                'user' => $user,
                            ])
                            ->send();

                    $this->Flash->success('An email with further instructions has been sent');
                    $this->request->data = [];
                }
                else
                {
                    $this->Flash->error('Failed, try again');
                }
            }
        }
    }

    //====================================================================
    // Reset password page
    public function resetPassword($identifier, $token)
    {
        $this->set('currentMenu', 'Login');

        if (is_null($identifier) || is_null($token))
        {
            $this->Flash->error('Incorrect URL');
            return $this->redirect(['action' => 'login']);
        }

        $key = $this->Users->SessionKeys->find()
                ->where(['identifier' => $identifier])
                ->andWhere(['token' => Security::hash($token, 'sha256', true)])
                ->first();

        if (is_null($key))
        {
            $this->Flash->error('Incorrect codes, request a new email');
            return $this->redirect(['action' => 'login']);
        }
        else
        {
            $date = date_create();
            $date->modify('-6 hours');

            if ($key->modified < $date)
            {
                $this->Flash->error('Your link has expired. Request a new one.');

                if (!$this->Users->SessionKeys->delete($key))
                {
                    if (Configure::read('debug'))
                        $this->Flash->error('Failed to delete session key from database');
                }

                return $this->redirect(['action' => 'login']);
            }

            if ($this->request->is('post'))
            {
                if ($this->request->data['newpassword'] === $this->request->data['confirm_newpassword'])
                {
                    $user = $this->Users->get($key->user_id);

                    $user = $this->Users->PatchEntity($user, ['password' => $this->request->data['newpassword']]);

                    if ($this->Users->save($user))
                    {
                        $this->Flash->success('Password changed, try logging in');

                        if (!$this->Users->SessionKeys->delete($key))
                        {
                            if (Configure::read('debug'))
                                $this->Flash->error('Failed to delete session key from database');
                        }

                        return $this->redirect(['action' => 'login']);
                    }
                    else
                        $this->Flash->error('Failed to save password');
                }
                else
                    $this->Flash->error('Passwords do not match');
            }
        }
    }

}
