<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Karma Controller
 *
 * @property \App\Model\Table\KarmaTable $Karma
 */
class KarmaController extends AppController
{

    //====================================================================
    public function isAuthorized($user)
    {
        if ($this->request->params['action'] == 'bulkAdd')
        {
            if (!isset($user['role']) || !in_array($user['role'], ['editor', 'admin']))
                return false;
        }

        if (isset($user))
            return true;

        return parent::isAuthorized($user);
    }

    //====================================================================
    //Personal Page
    public function view($id = NULL)
    {
        if (is_null($id))
            $id = $this->Auth->user('id');

        if ($id == $this->Auth->user('id'))
            $this->set('currentMenu', 'Home');
        else
            $this->set('currentMenu', 'Participants');

        //Form --------------------------------------------------------
        $committees = $this->Karma->Committees->find('list')->order(['name' => 'asc']);
        $this->set(compact('committees'));

        $user = $this->Karma->Users->get($id);
        $this->set(compact('user'));

        if ($this->request->is('post'))
        {
            if (in_array($this->Auth->user('role'), ['admin', 'editor']))
            {
                $this->request->data['approved'] = 1;
            }
            else
            {
                $this->request->data['approved'] = 0;
            }

            $this->request->data['user_id'] = $id;
            $new_karma = $this->Karma->newEntity($this->request->data);

            if ($this->Karma->save($new_karma))
            {
                $this->Flash->success('New event has been added');
                return $this->redirect(['action' => 'view', $id]);
            }
            else
                $this->Flash->error('Failed to add new event');
        }

        $this->set('karmaUser', $this->Karma->Users->get($id));

        //List --------------------------------------------------------

        $this->paginate = [
            'limit' => 10,
            'order' => ['Karma.date' => 'desc'],
            'sortWhitelist' => ['Committees.name', 'Karma.points', 'Karma.date', 'Karma.created']
        ];

        $karmas = $this->Karma->find('all')
                ->contain(['Committees'])
                ->where(['user_id' => $id]);

        $this->set('karmas', $this->paginate($karmas));

        $total_karma = $this->Karma->find('all')
                ->select(['karmascore' => 'SUM(points)'])
                ->where(['Karma.user_id' => $id])
                ->andWhere(['approved' => '1'])
                ->group('Karma.user_id')
                ->first();

        if (!is_null($total_karma))
            $this->set('total_karma', $total_karma->karmascore);
        else
            $this->set('total_karma', 0);

        $karma_groups = $this->Karma->find('all')
                ->select(['karmascore' => 'SUM(points)', 'committee_name' => 'Committees.name'])
                ->where(['Karma.user_id' => $id])
                ->andWhere(['Karma.approved' => '1'])
                ->leftJoinWith('Committees')
                ->group('Committees.id');

        debug($karma_groups->toArray());

        $this->set(compact('karma_groups'));
    }

    //====================================================================
    public function bulkAdd()
    {
        $this->set('currentMenu', 'Participants');

        $committees = $this->Karma->Committees->find('list');
        $this->set(compact('committees'));

        $users = $this->Karma->Users
                ->find('list', ['groupField' => 'committee_id'])
                ->toArray();

        $committees = $committees->toArray();

        foreach ($users as $i => $group)
        {
            $users[$committees[$i]] = $group;
            unset($users[$i]);
        }

        $this->set('users', $users);

        if ($this->request->is('post'))
        {
                $users = $this->request->data['Karma']['user_id'];

                $newData = $this->request->data['Karma'];

                $complete = true;

                foreach ($users as $user)
                {
                    $newData['user_id'] = $user;
                    $new_karma = $this->Karma->newEntity($newData);

                    if (!$this->Karma->save($new_karma))
                    {
                        $this->Flash->error('Failed to save event for user id: ' . $new_karma->user_id);
                        $complete = false;
                    }
                }
                if ($complete)
                    $this->Flash->success('Successfully saved all the events');
        }
    }

    //====================================================================
    public function delete($id = null, $user_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $karma = $this->Karma->get($id);
        if ($this->Karma->delete($karma))
        {
            $this->Flash->success('The event has been deleted.');
        }
        else
        {
            $this->Flash->error('The event could not be deleted. Please, try again.');
        }

        return $this->redirect(['action' => 'view', $user_id]);
    }

    //====================================================================
    public function edit($id = null, $user_id = null)
    {
        if ($user_id == $this->Auth->user('id'))
            $this->set('currentMenu', 'Home');
        else
            $this->set('currentMenu', 'Participants');

        $karma = $this->Karma->get($id);
        $committees = $this->Karma->Committees->find('list');

        if ($this->request->is(['post', 'put', 'patch']))
        {
            if (!in_array($this->Auth->user('role'), ['admin', 'editor']))
                $this->request->data['approved'] = 0;
            $karma = $this->Karma->patchEntity($karma, $this->request->data);

            if ($this->Karma->save($karma))
            {
                $this->Flash->success('Changed event succesfully');
                return $this->redirect(['action' => 'view', $user_id]);
            }
            else
                $this->Flash->error('Failed to edit event');
        }

        $this->set(compact('karma'));
        $this->set(compact('committees'));
    }

    //====================================================================
    //Make an approval list
    public function approve()
    {
        $this->set('currentMenu', 'Approve');

        $this->loadModel('Karma');

        $karmaQuery = $this->Karma->find()
                ->where(['approved !=' => 1])
                ->order(['Committees.name' => 'ASC', 'Users.firstname' => 'ASC', 'Karma.date' => 'DESC'])
                ->contain(['Committees', 'Users']);

        $karmas = [];

        foreach ($karmaQuery as $karma)
        {
            $karmas[$karma->committee->name][] = $karma;
        }

        $this->set(compact('karmas'));
    }

    //====================================================================
    //Change approval status
    public function setApprove($id = NULL, $status = NULL)
    {
        $this->request->allowMethod(['post']);

        $this->loadModel('Karma');

        $karma = $this->Karma->get($this->request->getData('karma_id'));

        $karma->approved = $this->request->getData('state');

        if ($this->Karma->save($karma))
        {
            http_response_code(201);
            die();
        }
        else
        {
            http_response_code(502);
            die();
        }
    }

}
