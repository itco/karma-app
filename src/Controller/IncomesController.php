<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Incomes Controller
 *
 * @property \App\Model\Table\IncomesTable $Incomes
 */
class IncomesController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        $this->set('currentMenu', 'Incomes');
    }

    public function isAuthorized($user)
    {
        if (isset($user))
        {
            if (in_array($this->request->params['action'], ['table', 'index']))
                return true;
            elseif (isset($user['role']) && $user['role'] == 'editor')
                return true;
        }

        return parent::isAuthorized($user);
    }

    public function index()
    {
        $total = $this->Incomes->find()
                ->select(['total' => 'SUM(money)'])
                ->where(['definitive' => true])
                ->first();

        $this->set('total', $total->total);

        $totalish = $this->Incomes->find()
                ->select(['total' => 'SUM(money)'])
                ->where(['definitive' => false])
                ->first();
        
        $this->set('totalish', $totalish->total);


        $budget = Configure::read('Stacks.budget');

        $this->set(compact('budget'));
    }

    public function table()
    {
        if ($this->request->is('post'))
        {
            $new_budget = $this->request->data['budget'];
            if (!empty($new_budget) && $new_budget > 0 && in_array($this->Auth->user('role'), ['editor', 'admin']))
            {
                Configure::write('Stacks.budget', $new_budget);
                Configure::dump('stacks', 'default', ['Stacks']);
                $this->Flash->success('Budget updated');
            }
            else
                $this->Flash->error('Error');
        }

        $this->paginate = [
            'order' => ['created' => 'DESC'],
        ];

        $incomes = $this->paginate($this->Incomes);
        $this->set(compact('incomes'));

        $budget = Configure::read('Stacks.budget');
        $this->set('budget', $budget);
    }

    public function add()
    {
        $income = $this->Incomes->newEntity();
        if ($this->request->is('post'))
        {
            $income = $this->Incomes->patchEntity($income, $this->request->data);
            if ($this->Incomes->save($income))
            {
                return $this->redirect(['action' => 'table']);
            }
            else
            {
                $this->Flash->error('The income could not be saved. Please, try again.');
            }
        }
        $this->set(compact('income'));
        $this->set('_serialize', ['income']);
    }

    public function edit($id = null)
    {
        $income = $this->Incomes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put']))
        {
            $income = $this->Incomes->patchEntity($income, $this->request->data);
            if ($this->Incomes->save($income))
            {
                return $this->redirect(['action' => 'table']);
            }
            else
            {
                $this->Flash->error('The income could not be saved. Please, try again.');
            }
        }

        $this->set(compact('income'));
        $this->set('_serialize', ['income']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $income = $this->Incomes->get($id);
        if ($this->Incomes->delete($income))
        {
            
        }
        else
        {
            $this->Flash->error('The income could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }

}
