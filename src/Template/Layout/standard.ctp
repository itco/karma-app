<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Karma-app
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('bootstrap.css') ?>
        <?= $this->Html->css('style.css') ?>

        <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('bootstrap.js') ?>

        <?= $this->fetch('script') ?>
    </head>
    <body>

        <div id="page">

            <div id="banner">
                <?= $this->Html->image('banner.jpg', ['width' => '100%']) ?>
            </div>


            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <?php

                        echo $this->element('menubutton', ['name' => 'Webapps &nbsp;&nbsp;&nbsp;&nbsp;', 'icon' => 'menu-left', 'tag' => 'webapps', 'link' => 'https://webapps.astatine.utwente.nl']);

                        echo $this->element('menubutton', ['name' => 'Home', 'link' => ['controller' => 'Karma', 'action' => 'view']]);

                        echo $this->element('menubutton', ['name' => 'Participants', 'link' => ['controller' => 'Users', 'action' => 'table']]);

                        if (isset($authUser) && in_array($authUser['role'], ['admin', 'editor']))
                                echo $this->element('menubutton', ['name' => 'Approve', 'link' => ['controller' => 'Karma', 'action' => 'approve']]);

                        echo $this->element('menubutton', ['name' => 'Cases', 'link' => ['controller' => 'Committees', 'action' => 'table']]);

                        echo $this->element('menubutton', ['name' => 'Stacks-Tracker', 'tag' => 'Incomes', 'link' => ['controller' => 'Incomes', 'action' => 'index']]);

                        if (isset($authUser) && $authUser['role'] == 'admin')
                        {
                            echo $this->element('dropdownbutton', ['name' => 'Manage', 'options' => [
                                    ['name' => 'Cases', 'tag' => 'Man_Committees', 'link' => ['controller' => 'Committees', 'action' => 'index']],
                                    ['name' => 'Users', 'tag' => 'Man_Users', 'link' => ['controller' => 'Users', 'action' => 'index']],
                            ]]);
                        }
                        if (isset($authUser) && $authUser['role'] == 'editor')
                        {
                            echo $this->element('menubutton', ['name' => 'Manage cases',  'link' => ['controller' => 'Committees', 'action' => 'index']]);
                        }
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (!isset($authUser))
                        {
                            echo $this->element('menubutton', ['icon' => 'user', 'name' => 'Sign up', 'link' => ['controller' => 'Users', 'action' => 'add']]);
                            echo $this->element('menubutton', ['icon' => 'log-in', 'name' => 'Login', 'link' => ['controller' => 'Users', 'action' => 'login']]);
                        }
                        else
                        {
                            echo $this->element('dropdownbutton', ['name' => $authUser['username'] . (($authUser['role'] == 'admin') ? ' (admin)' : ''), 'tag' => 'User', 'options' => [
                                    ['icon' => 'pencil', 'name' => 'Change Password', 'link' => ['controller' => 'Users', 'action' => 'changepassword']],
                                    ['icon' => 'edit', 'name' => 'Edit', 'link' => ['controller' => 'Users', 'action' => 'edit']],
                                    ['icon' => 'log-out', 'name' => 'Logout', 'link' => ['controller' => 'Users', 'action' => 'logout']],
                            ]]);
                        }
                        ?>
                    </ul>
                </div>
            </nav>

            <?= $this->Flash->render() ?>
            <?= $this->Flash->render('auth') ?>
            <div class="container">
                <?= $this->fetch('content') ?>
            </div>

        </div>
    </body>
</html>
