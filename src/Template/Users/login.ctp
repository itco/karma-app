<?= $this->Form->create() ?>
<fieldset>
    <legend>Username and Password</legend>
    <?= $this->Form->input('username', ['type' => 'text']) ?>
    <?= $this->Form->input('password', ['type' => 'password']) ?>
    <?=
    $this->Form->input('remember_me', [
        'type' => 'checkbox',
        'label' => 'Remember Me',
        'value' => 1,
        'checked' => true
    ])
    ?>
    <br><br>
</fieldset>
<?= $this->Form->button('Login'); ?>
<?= $this->Form->end() ?>

<p class="forgot-password"><?= $this->Html->link('Forgot your password or username?', ['controller' => 'Users', 'action' => 'forgotPassword']) ?></p>
