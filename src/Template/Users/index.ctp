<h3>Users</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('username') ?></th>
            <th><?= $this->Paginator->sort('firstname') ?></th>
            <th><?= $this->Paginator->sort('lastname') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('committee_id') ?></th>
            <th><?= $this->Paginator->sort('role') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><span title="id = <?= $user->id ?>"><?= h($user->username) ?></span></td>
                <td><?= h($user->firstname) ?></td>
                <td><?= h($user->lastname) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->telephone) ?></td>
                <td><?= $user->has('committee') ? $this->Html->link($user->committee->name, ['controller' => 'Committees', 'action' => 'view', $user->committee->id]) : '' ?></td>
                <td><?= h($user->role) ?></td>
                <td><?= h($user->created->format('d-m-Y H:i')) ?></td>
                <td class="actions">
                    <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash" title="Delete"></span>',
                            ['action' => 'delete', $user->id],
                            ['confirm' => 'Are you sure you want to delete this user?', 'escape' => false]) ?>
                    <?= $this->Html->link('<span class="glyphicon glyphicon-edit" title="Edit"></span>',
                            ['action' => 'edit', $user->id],
                            ['escape' => false]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< previous') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('next >') ?>
    </ul>
</div>