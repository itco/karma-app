<?php
$this->Html->script('https://www.google.com/recaptcha/api.js', ['block' => true]);
?>

<?= $this->Form->create() ?>
<fieldset>
    <legend>Retrieve login information</legend>
    <?= $this->Form->input('email', ['type' => 'email']) ?>
    <br>
</fieldset>

<div class="g-recaptcha" data-sitekey="<?= $siteKey ?>"></div><br>

<?= $this->Form->button('Submit'); ?>
<?= $this->Form->end() ?>