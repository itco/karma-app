<?php if (in_array($authUser['role'], ['admin', 'editor'])) : ?>
    <ul>
        <li><?= $this->Html->link('Bulk Add Events', ['controller' => 'Karma', 'action' => 'bulk_add']) ?></li>
    </ul>
<?php endif; ?>

<h3>Participants</h3>
<p><i>Note: Only participants with a karmascore greater than 0 are shown</i></p>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('firstname') ?></th>
            <th><?= $this->Paginator->sort('lastname') ?></th>
            <th><?= $this->Paginator->sort('karmascore') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($participants as $participant): ?>
            <tr>
                <td>
                    <?= $this->Html->link($participant->firstname, ['controller' => 'Karma', 'action' => 'view', $participant->id]) ?>
                </td>
                <td>
                    <?= $this->Html->link($participant->lastname, ['controller' => 'Karma', 'action' => 'view', $participant->id]) ?>
                </td>
                <td><?= $participant->karmascore ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< previous') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('next >') ?>
    </ul>
</div>
