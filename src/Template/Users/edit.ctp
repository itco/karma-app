<ul>
    <li><?=
        $this->Form->postLink(
                'Delete Account', ['action' => 'delete', $user->id], ['confirm' => 'Are you sure you want to delete your account? This cannnot be undone and could break the karma system']
        )
        ?></li>
</ul>
<?= $this->Form->create($user) ?>
<fieldset>
    <legend>Edit Profile</legend>
    <?php
    echo $this->Form->input('username');
    echo $this->Form->input('firstname');
    echo $this->Form->input('lastname');
    echo $this->Form->input('email');
    echo $this->Form->input('telephone');
    echo $this->Form->input('committee_id');
    if (isset($authUser['role']) && $authUser['role'] == 'admin')
    {
        echo $this->Form->input('role', [
            'type' => 'select',
            'options' => ['admin' => 'admin', 'user' => 'user', 'editor' => 'editor']
            ]);
    }
    else
    {
        echo $this->Form->input('role', ['label' => 'Authorization Level', 'disabled' => true]);
    }
    ?>
</fieldset>
<?= $this->Form->button('Submit') ?>
<?= $this->Form->end() ?>
