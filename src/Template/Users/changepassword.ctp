
<?= $this->Form->create() ?>
<fieldset>
    <legend>Change password</legend>
    <?= $this->Form->input('password', ['type' => 'password', 'label' => 'Current Password']) ?>
    <?= $this->Form->input('newpassword', ['type' => 'password', 'label' => 'New Password']) ?>
    <?= $this->Form->input('confirm_newpassword', ['type' => 'password', 'label' => 'New Password (Confirm)']) ?>
    <br><br>
</fieldset>
<?= $this->Form->button('Submit'); ?>
<?= $this->Form->end() ?>