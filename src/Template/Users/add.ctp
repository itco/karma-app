<?= $this->Form->create($user) ?>
<fieldset>
    <legend>Add User</legend>
    <?php
    echo $this->Form->input('firstname');
    echo $this->Form->input('lastname');
    echo $this->Form->input('username');
    echo $this->Form->input('password');
    echo $this->Form->input('confirm_password', ['type' => 'password']);
    echo $this->Form->input('email');
    echo $this->Form->input('telephone');
    echo $this->Form->input('committee_id', ['options' => $committees]);
    ?>
</fieldset>
<?= $this->Form->button('Submit') ?>
<?= $this->Form->end() ?>