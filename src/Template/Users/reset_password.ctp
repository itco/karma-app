<?= $this->Form->create() ?>
<fieldset>
    <legend>Set new password</legend>
    <?= $this->Form->input('newpassword', ['type' => 'password', 'label' => 'New Password']) ?>
    <?= $this->Form->input('confirm_newpassword', ['type' => 'password', 'label' => 'New Password (Confirm)']) ?>
    <br>
</fieldset>
<?= $this->Form->button('Submit'); ?>
<?= $this->Form->end() ?>