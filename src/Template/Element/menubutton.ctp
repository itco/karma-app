<?php
if (!isset($tag))
{
    $tag = $name;
}

if (isset($icon))
{
    $icon = '<span class="glyphicon glyphicon-' . $icon . '" title="' . $name . '"></span>';
}
?>

<li <?= ($currentMenu == $tag) ? 'class="active"' : '' ?>>
    <?= $this->Html->link(((isset($icon)) ? $icon : '') . $name, $link, ['escape' => false]) ?>
</li>