<?php
$isActive = false;
foreach ($options as $option)
{
    if (isset($option['tag']))
    {
        if ($option['tag'] == $currentMenu)
        {
            $isActive = true;
        }
    }
    elseif ($option['name'] == $currentMenu)
        $isActive = true;
}
?>

<li <?= ($isActive) ? 'class="dropdown active"' : 'class="dropdown"' ?>>
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <?= $name ?> <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <?php
        //debug($options);
        foreach ($options as $option)
        {
            echo $this->element('menubutton', $option);
        }
        ?>
    </ul>
</li>