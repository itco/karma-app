<?php $this->Html->script('approve.js', ['block' => true]) ?>

<script>
    // Make the webroot (e.g. "webapps/karma/") available to javascript
    var webroot = "<?= $this->request->webroot ?>";
</script>

<h3>Approve Karma</h3>
<br>

<?php foreach ($karmas as $com => $karmas_com) : ?>

    <h4><?= $com ?></h4>

    <table class="table table-karma">
        <thead>
            <tr>
                <th>Name</th>
                <th>Karma</th>
                <th>Date</th>
                <th>Comment</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($karmas_com as $karma):
                if ($karma->approved == 0)
                    $class = 'class="waiting"';
                elseif ($karma->approved == 2)
                    $class = 'class="denied"';
                else
                    $class = '';
                ?>
                <tr <?= $class ?> id="karma-entry-<?= $karma->id ?>">
                    <td>
                        <?= $this->Html->link($karma->user->firstname . ' ' . $karma->user->lastname, ['controller' => 'Karma', 'action' => 'view', $karma->user->id]) ?>
                    </td>
                    <td><?= $karma->points ?></td>
                    <td><?= $karma->date->format('d-m-Y') ?></td>
                    <td><?= $karma->comment ?></td>
                    <td>
                        <?=
                        $this->Form->button(
                                '<span class="glyphicon glyphicon-ok-circle" title="Approved"></span>', [
                            'type' => 'button',
                            'class' => 'btn-link',
                            'escape' => false,
                            'onclick' => 'setApprove(' . $karma->id . ', 1)']
                        )
                        ?>
                        <?=
                        $this->Form->button(
                                '<span class="glyphicon glyphicon-ban-circle" title="Pending"></span>', [
                            'type' => 'button',
                            'class' => 'btn-link',
                            'escape' => false,
                            'onclick' => 'setApprove(' . $karma->id . ', 0)']
                        )
                        ?>
                        <?=
                        $this->Form->button(
                                '<span class="glyphicon glyphicon-remove-circle" title="Disapproved"></span>', [
                            'type' => 'button',
                            'class' => 'btn-link',
                            'escape' => false,
                            'onclick' => 'setApprove(' . $karma->id . ', 2)']
                        )
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php endforeach; ?>

<p>
    * Karma entry needs to be approved before it is counted <br>
    ** Your karma entry was not approved and is not counted, modify or delete it
</p>
