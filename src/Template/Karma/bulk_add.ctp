<?= $this->Form->create() ?>
<fieldset>
    <legend>Add Event In Bulk</legend>
    <?php
    echo $this->Form->input('Karma.user_id', ['multiple' => true]);
    echo $this->Form->input('Karma.points', ['type' => 'number', 'label' => 'Hours (per person)']);
    echo $this->Form->input('Karma.committee_id');
    echo $this->Form->input('Karma.date', ['type' => 'date']);
    echo $this->Form->input('Karma.comment', ['type' => 'textarea', 'rows' => 2, 'cols' => 50]);

    echo $this->Form->hidden('Karma.approved', ['value' => 1]);
    ?>
</fieldset>
<?= $this->Form->button('Submit') ?>
<?= $this->Form->end() ?>
