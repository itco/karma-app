<?= $this->Form->create($karma) ?>
<fieldset>
    <legend>Edit Event</legend>
    <?php
    echo $this->Form->input('committee_id', ['label' => 'Case']);
    echo $this->Form->input('date', ['type' => 'date']);
    echo $this->Form->input('comment', ['type' => 'textarea', 'rows' => 2, 'cols' => 50]);
    ?>
</fieldset>
<?= $this->Form->button('Submit') ?>
<?= $this->Form->end() ?>
