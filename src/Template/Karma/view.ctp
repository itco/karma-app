<div class="row">
    <div class="col-md-4">
        <h2><?= $karmaUser['full_name'] ?></h2>
    </div>
    <div class="col-md-8">
        <h2><?= 'Karma: ' . $total_karma ?></h2>
    </div>
</div>
<br><br>
<div class="row">

    <!-- FORM -->
    <div class="col-md-5 col-sm-12 col-md-push-7">

        <div id="karmaform">
            <?= $this->Form->create() ?>
            <fieldset>
                <legend>Add Event</legend>
                <?php
                echo $this->Form->input('points', ['type' => 'number', 'label' => 'Hours']);
                echo $this->Form->input('committee_id', ['value' => $user->committee_id, 'label' => 'Case']);
                echo $this->Form->input('date', ['type' => 'date']);
                echo $this->Form->input('comment', ['type' => 'textarea', 'rows' => 2, 'cols' => 50]);
                ?>
            </fieldset>
            <?= in_array($authUser['role'], ['editor', 'admin']) ? '' : '<p><i>Note: Your karma events have to be approved first</i></p>' ?>
            <?= $this->Form->button('Submit') ?>
            <?= $this->Form->end() ?>

            <br> <br>

            <h5><?= $this->Html->link('> Go to bulk add', ['controller' => 'Karma', 'action' => 'bulk_add']) ?></h5>

        </div>

        <!-- List -->
        <div id="statform">
            <h4>Karma per case</h4>
            <table class="table table-striped table-stats">
                <tbody>
                    <?php foreach ($karma_groups as $group) : ?>

                        <tr>
                            <th><?= $group->committee_name ?></th>
                            <td><?= $group->karmascore ?></td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- TABLE -->
    <div class="col-md-7 col-sm-12 col-md-pull-5">
        <table class="table table-striped table-karma">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('Committees.name', 'Committee') ?></th>
                    <th><?= $this->Paginator->sort('Karma.points', 'Karma') ?></th>
                    <th>Comment</th>
                    <th><?= $this->Paginator->sort('Karma.date', 'Date', ['direction' => 'desc']) ?></th>
                    <th><?= $this->Paginator->sort('Karma.created', 'Added', ['direction' => 'desc']) ?></th>
                    <th class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($karmas as $karma):

                    if ($karma->approved == 0)
                        $class = 'class="waiting"';
                    elseif ($karma->approved == 2)
                        $class = 'class="denied"';
                    else
                        $class = '';
                    ?>
                    <tr <?= $class ?>>
                        <td><?= $this->Html->link($karma->committee->name, ['controller' => 'Committees', 'action' => 'view', $karma->committee->id]) ?></td>
                        <td><?= $karma->points ?></td>
                        <td><?= $karma->comment ?></td>
                        <td><?= $karma->date->format('d-m-Y') ?></td>
                        <td><?= $karma->created->format('d-m-Y<\b\\r>H:i') ?></td>
                        <td class="actions">
                            <?= $this->Html->link('<span class="glyphicon glyphicon-edit" title="Edit/' . $karma->id . '"></span>', ['action' => 'edit', $karma->id, $karmaUser['id']], ['escape' => false]) ?>
                            <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash" title="Delete"></span>', ['action' => 'delete', $karma->id, $karmaUser['id']], ['confirm' => 'Are you sure you want to delete this entry?', 'escape' => false]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p>
            * This karma entry needs to be approved before it is counted <br>
            ** This karma entry was not approved and is not counted, modify or delete it
        </p>

        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< previous') ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('next >') ?>
            </ul>
        </div>
    </div>

</div>
