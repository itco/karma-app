<p>Dear <?= $user['full_name'] ?>,</p>

<p>
    This email was sent because you have requested password reset.<br><br>
    Your username is: <b><?= $user['username'] ?></b><br><br>

    Click on the following link to recover your password:<br>
    <?php
    $link = $this->Url->build(['controller' => 'Users', 'action' => 'resetPassword', $identifier, $token], true);

    echo $this->Html->link($link, $link);
    ?>
    <br><br>
    
    This link will remain active for 6 hours. If e.g. you only needed the username, you can consider this email as not sent, after maybe deleting it.<br>

    This email was automatically generated.<br>
    
    If you did not request this password change, consider checking your email and your account safety and consier contacting the website admin.<br><br>
    
    Regards
</p>