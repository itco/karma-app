<?= $this->Form->create($income) ?>
<fieldset>
    <legend>Add Income</legend>
    <?php
    echo $this->Form->input('name');
    echo $this->Form->input('money');
    echo $this->Form->input('comment');
    echo $this->Form->input('definitive', ['checked' => true, 'label' => 'Definitive (basically in the pocket)']);
    ?>
</fieldset>
<?= $this->Form->button('Submit') ?>
<?= $this->Form->end() ?>