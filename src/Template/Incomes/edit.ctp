<?= $this->Form->create($income) ?>
<fieldset>
    <legend><?= __('Edit Income') ?></legend>
    <?php
    echo $this->Form->input('name');
    echo $this->Form->input('money');
    echo $this->Form->input('comment');
    echo $this->Form->input('definitive', ['label' => 'Definitive (basically in the pocket)']);
    ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>