<div class="incomes index large-9 medium-8 columns content">
    
    <?= $this->Form->create() ?>
    <?= $this->Form->input('budget', ['type' => 'number', 'step' => '0.01', 'label' => 'Goal (€)', 'value' => $budget, 'min' => 0]) ?>
    <?= $this->Form->button('submit') ?>
    <?= $this->Form->end() ?>
    
    <h3>Flap-entries</h3>
    <br>
    <h4><?= $this->Html->link('Add some green', ['action' => 'add']) ?></h4>
    <br>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('money') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('created', 'Added', ['direction' => 'DESC']) ?></th>
                <th>Definitive</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($incomes as $income): ?>
            <tr>
                <td><?= h($income->name) ?></td>
                <td><?= $this->Number->currency($income->money, 'EUR') ?></td>
                <td><?= h($income->comment) ?></td>
                <td><?= $income->created->format('d-m-Y') ?></td>
                <td><?= $income->definitive ? 'Yes' : 'No' ?></td>
                <td class="actions">
                    <?= $this->Html->link('<span class="glyphicon glyphicon-edit" title="Edit"></span>', ['action' => 'edit', $income->id], ['escape' => false]) ?>
                    <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash" title="Delete"></span>', ['action' => 'delete', $income->id], ['confirm' => 'Delete?', 'escape' => false]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< previous') ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('next >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
