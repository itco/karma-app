<h2>Stacks-Tracker</h2>

<div id="stacktracker">
    <div class="bar" id="bar-1">
        <?= $this->Number->currency($total, 'EUR') ?>
    </div>
    <?php if ($totalish > 0) : ?>
        <div class="bar" id="bar-2">
            <?= $this->Number->currency($total + $totalish, 'EUR') ?>
        </div>
    <?php endif; ?>
    <div class="budget">
        <?= $this->Number->currency($budget, 'EUR') ?>
    </div>
</div>


<h4>
    <?= $this->Html->link('<span class="glyphicon glyphicon-edit" title="Edit"></span>List', ['action' => 'table'], ['escape' => false]) ?>
</h4>

<script type="text/javascript">
    window.onload = function ()
    {
        var bar = document.getElementById("bar-1");

        var w = <?= $total / $budget * 100 ?>;
        if (w > 100)
            w = 100;

        bar.style.width = w + "%";


        bar = document.getElementById("bar-2");

        var w = <?= ($totalish) / $budget * 100 ?>;
        if (w > 100)
            w = 100;

        bar.style.width = w + "%";
    };
</script>