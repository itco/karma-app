<h3>Cases</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Total Karma</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($committees as $committee): ?>
            <tr>
                <td>
                    <?= $this->Html->link($committee->name, ['controller' => 'Committees', 'action' => 'view', $committee->id]) ?>
                </td>
                <td><?= $committee->totalkarma ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
