<h3><?= $committee->name ?></h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('Users.firstname', 'Name') ?></th>
            <th><?= $this->Paginator->sort('Karma.points', 'Karma') ?></th>
            <th><?= $this->Paginator->sort('Karma.comment', 'Comment') ?></th>
            <th><?= $this->Paginator->sort('Karma.date', 'Date') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($karmas as $karma): ?>
            <tr>
                <td>
                    <?= $this->Html->link($karma->user->firstname, ['controller' => 'Karma', 'action' => 'view', $karma->user->id]) ?>
                </td>
                <td><?= $karma->points ?></td>
                <td><?= $karma->comment ?></td>
                <td><?= $karma->date->format('d-m-Y') ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< previous') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('next >') ?>
    </ul>
</div>
