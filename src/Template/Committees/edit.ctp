<ul>
    <li><?=
        $this->Form->postLink(
                __('Delete'), ['action' => 'delete', $committee->id], ['confirm' => __('Are you sure you want to delete case # {0}?', $committee->id)]
        )
        ?></li>
</ul>
<?= $this->Form->create($committee) ?>
<fieldset>
    <legend>Edit Case</legend>
    <?php
    echo $this->Form->input('name');
    echo $this->Form->input('email');
    ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
