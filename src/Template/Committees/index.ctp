<ul>
    <li><?= $this->Html->link('New Case', ['action' => 'add']) ?></li>
</ul>
<h3>Cases</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th class="actions">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($committees as $committee): ?>
            <tr>
                <td><?= h($committee->name) ?></td>
                <td><?= h($committee->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<span class="glyphicon glyphicon-edit" title="Edit"></span>', ['action' => 'edit', $committee->id], ['escape' => false]) ?>
                    <?= $this->Form->postLink('<span class="glyphicon glyphicon-trash" title="Delete"></span>',
                            ['action' => 'delete', $committee->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $committee->id), 'escape' => false]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< previous') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('next >') ?>
    </ul>
</div>
